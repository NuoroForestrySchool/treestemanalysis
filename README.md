Processing stem analysis data in two steps:
1 - verification of possible missed alignement between reading on the the same section and then between sections,
2 - production of standardised profiles for all ages.