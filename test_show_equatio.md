---
title: "Tset of initial part of https://rpubs.com/ma_mohamadi/475555"
output: html_notebook
---

It should produce nice to print formulas
... but I could not get the 'julia' engine to work!


```r
# libraries
## Needed packages
packages <- c("tidyverse",
              "JuliaCall",
              "magrittr",
              "broom",
              "car")
## check for not installed packages and install them
not_installed <- packages[!(packages %in% installed.packages())]
### check if not_installed is not empty
if( length(not_installed) != 0 )
install.packages(not_installed, dependencies = TRUE)
## load all needed packages
lapply(packages, function(x) library(x, character.only = TRUE))
```

```
## -- Attaching packages ---------------------------------- tidyverse 1.2.1 --
```

```
## v ggplot2 3.2.0     v purrr   0.2.5
## v tibble  1.4.2     v dplyr   0.7.6
## v tidyr   0.8.1     v stringr 1.3.1
## v readr   1.1.1     v forcats 0.3.0
```

```
## Warning: package 'ggplot2' was built under R version 3.5.3
```

```
## -- Conflicts ------------------------------------- tidyverse_conflicts() --
## x dplyr::filter() masks stats::filter()
## x dplyr::lag()    masks stats::lag()
```

```
## Warning: package 'JuliaCall' was built under R version 3.5.3
```

```
## 
## Attaching package: 'magrittr'
```

```
## The following object is masked from 'package:purrr':
## 
##     set_names
```

```
## The following object is masked from 'package:tidyr':
## 
##     extract
```

```
## Loading required package: carData
```

```
## 
## Attaching package: 'car'
```

```
## The following object is masked from 'package:dplyr':
## 
##     recode
```

```
## The following object is masked from 'package:purrr':
## 
##     some
```

```
## [[1]]
##  [1] "forcats"   "stringr"   "dplyr"     "purrr"     "readr"    
##  [6] "tidyr"     "tibble"    "ggplot2"   "tidyverse" "knitr"    
## [11] "stats"     "graphics"  "grDevices" "utils"     "datasets" 
## [16] "methods"   "base"     
## 
## [[2]]
##  [1] "JuliaCall" "forcats"   "stringr"   "dplyr"     "purrr"    
##  [6] "readr"     "tidyr"     "tibble"    "ggplot2"   "tidyverse"
## [11] "knitr"     "stats"     "graphics"  "grDevices" "utils"    
## [16] "datasets"  "methods"   "base"     
## 
## [[3]]
##  [1] "magrittr"  "JuliaCall" "forcats"   "stringr"   "dplyr"    
##  [6] "purrr"     "readr"     "tidyr"     "tibble"    "ggplot2"  
## [11] "tidyverse" "knitr"     "stats"     "graphics"  "grDevices"
## [16] "utils"     "datasets"  "methods"   "base"     
## 
## [[4]]
##  [1] "broom"     "magrittr"  "JuliaCall" "forcats"   "stringr"  
##  [6] "dplyr"     "purrr"     "readr"     "tidyr"     "tibble"   
## [11] "ggplot2"   "tidyverse" "knitr"     "stats"     "graphics" 
## [16] "grDevices" "utils"     "datasets"  "methods"   "base"     
## 
## [[5]]
##  [1] "car"       "carData"   "broom"     "magrittr"  "JuliaCall"
##  [6] "forcats"   "stringr"   "dplyr"     "purrr"     "readr"    
## [11] "tidyr"     "tibble"    "ggplot2"   "tidyverse" "knitr"    
## [16] "stats"     "graphics"  "grDevices" "utils"     "datasets" 
## [21] "methods"   "base"
```

```r
# knitr options
knitr::opts_chunk$set(fig.pos = 'H', out.extra = '')
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(out.width = '75%', fig.align = 'center' ) 
knitr::opts_chunk$set(comment=NA)

julia_setup(JULIA_HOME = "C:\\Users\\ro\\AppData\\Local\\Julia-1.1.1\\bin\\", rebuild = T)
```

```
## Julia version 1.1.1 at location C:\Users\ro\AppData\Local\Julia-1.1.1\bin will be used.
```

```
## Loading setup script for JuliaCall...
```

```
## Finish loading setup script for JuliaCall.
```


```julia
# [Juila Code]

using SymPy

# initial variables

P, a_0, a_1, ν, Q, C, η, F, b_0, b_1, N, q, π = 
    symbols("P, a_0, a_1, ν, Q, C, η, F, b_0, b_1, N, q, π");

q = ( (a_0-b_0)+(ν-η) )/( a_1*(N+1)-2*b_1);

Q = N*q;

P = a_0 - a_1*Q + ν;

C = (b_0 - b_1*q + η)*q + F;

π = P*q - C; 

# substitute b_1 = 0

π = subs(π,b_1,0); 
simplify(π);

show(stdout, "text/latex", π)
```


