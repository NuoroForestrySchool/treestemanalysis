---
title: "R Notebook"
output: pdf_document
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 


```r
rm(list=ls())
library(lmtest) ; library(sandwich)
```

```
## Warning: package 'lmtest' was built under R version 3.5.3
```

```
## Loading required package: zoo
```

```
## 
## Attaching package: 'zoo'
```

```
## The following objects are masked from 'package:base':
## 
##     as.Date, as.Date.numeric
```

```
## Warning: package 'sandwich' was built under R version 3.5.2
```

```r
# data sim
library(texreg)
```

```
## Warning: package 'texreg' was built under R version 3.5.3
```

```
## Version:  1.36.23
## Date:     2017-03-03
## Author:   Philip Leifeld (University of Glasgow)
## 
## Please cite the JSS article in your publications -- see citation("texreg").
```

```r
set.seed(1)
x <- rnorm(1000)
y <- 5 + 2*x + rnorm(1000,0,40)
# regression
m1 <- lm(y~x)
summary(m1)
```

```
## 
## Call:
## lm(formula = y ~ x)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -129.94  -26.88   -0.55   30.22  145.77 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)    4.353      1.316   3.307 0.000977 ***
## x              2.257      1.272   1.774 0.076348 .  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 41.62 on 998 degrees of freedom
## Multiple R-squared:  0.003144,	Adjusted R-squared:  0.002145 
## F-statistic: 3.148 on 1 and 998 DF,  p-value: 0.07635
```

```r
# triple data
dat <- data.frame(x=c(x,x,x),y=c(y,y,y),g=c(1:1000,1:1000,1:1000))
# regressions
m2 <- lm(y~x, dat) # smaller StErrs
 
# cluster robust standard error function
robust.se <- function(model, cluster){
  require(sandwich)
  require(lmtest)
  M <- length(unique(cluster))
  N <- length(cluster)
  K <- model$rank
  dfc <- (M/(M - 1)) * ((N - 1)/(N - K))
  uj <- apply(estfun(model), 2, function(x) tapply(x, cluster, sum));
  rcse.cov <- dfc * sandwich(model, meat = crossprod(uj)/N)
  rcse.se <- coeftest(model, rcse.cov)
  return(list(rcse.cov, rcse.se))
}
 
m3 <- robust.se(m2,dat$g)[[2]] # StErrs now back to what they are
 
texreg(list(m1,m2,m2),
       caption="The Importance of Clustering Standard Errors",
       dcolumn=FALSE,
       model.names=c("M1","M2","M3"),
       override.se=list(summary(m1)$coef[,2],
                        summary(m2)$coef[,2],
                        m3[,2]),
       override.pval=list(summary(m1)$coef[,4],
                          summary(m2)$coef[,4],
                          m3[,4]))
```

```
## 
## \begin{table}
## \begin{center}
## \begin{tabular}{l c c c }
## \hline
##  & Model 1 & Model 2 & Model 3 \\
## \hline
## (Intercept) & $4.35^{***}$ & $4.35^{***}$ & $4.35^{***}$ \\
##             & $(1.32)$     & $(0.76)$     & $(1.32)$     \\
## x           & $2.26$       & $2.26^{**}$  & $2.26$       \\
##             & $(1.27)$     & $(0.73)$     & $(1.32)$     \\
## \hline
## R$^2$       & 0.00         & 0.00         & 0.00         \\
## Adj. R$^2$  & 0.00         & 0.00         & 0.00         \\
## Num. obs.   & 1000         & 3000         & 3000         \\
## RMSE        & 41.62        & 41.59        & 41.59        \\
## \hline
## \multicolumn{4}{l}{\scriptsize{$^{***}p<0.001$, $^{**}p<0.01$, $^*p<0.05$}}
## \end{tabular}
## \caption{The Importance of Clustering Standard Errors}
## \label{table:coefficients}
## \end{center}
## \end{table}
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
